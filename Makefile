CC = gcc
CFLAGS = -Wall -Wextra -march=native -O3
CLIBS = -lm
EXEC = tests

all: $(EXEC)

$(EXEC): tests.c random-float-generator.c random-float-generator.h
	$(CC) $(CFLAGS) $(CLIBS) random-float-generator.c tests.c -o $@

graphs: $(EXEC)
	./generate-graphs.sh

clean:
	$(RM) $(EXEC) *.dat *.png
