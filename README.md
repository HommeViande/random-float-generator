# Random float generator

Functions in C to generate floats of uniform law between 0 and 1 (excluded),
that is able to generate any float between 0 and 1.

Gnuplot is required to generate the benhmark graphs.

`make graphs` produces the graphs.
