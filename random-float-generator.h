#ifndef __RANDOM_FLOAT_GENERATOR_H__
#define __RANDOM_FLOAT_GENERATOR_H__

// Returns a random float
float random_float(void);

// Produces a float from a given seed
float random_float_seeds(long seeds[6]);

#endif
